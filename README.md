# news_aggregator

How to start

- using local only:
  - `cd app`
  - `npm install`
  - `npm start`
  - open http://localhost:3000/

- using Docker:
  - switch to root directory
  - `make build` (runs once)
  - `make up`
   ➜ app
    - `npm install` (runs once)
    - `npm start`
    - open http://localhost:4444