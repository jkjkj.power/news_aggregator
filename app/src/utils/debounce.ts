//TODO: Fix types, remove any
export const debounce = (cb: (value: any) => void, delay: number) => {
  let timeoutId: NodeJS.Timeout;

  return (value: any) => {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }

    timeoutId = setTimeout(() => cb(value), delay);
  };
};
