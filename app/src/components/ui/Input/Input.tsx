import { ChangeEvent, FC } from "react";

import "./Input.styles.css";

type Props = {
  type?: string;
  placeholder?: string;
  onChange: (value: string) => void;
};

export const Input: FC<Props> = ({
  type = "string",
  placeholder,
  onChange,
}) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value);
  };

  return (
    <input
      placeholder={placeholder}
      className="input"
      onChange={handleChange}
      type={type}
    />
  );
};
