import { FC } from "react";

import "./Card.styles.css";

type Props = {
  title?: string;
  name?: string;
  url?: string;
  urlToImage?: string;
  description?: string;
  articleDate?: string;
};

export const Card: FC<Props> = ({
  title,
  url,
  name,
  urlToImage,
  description,
  articleDate,
}) => {
  const onCardClick = () => window.open(url);

  return (
    <div onClick={onCardClick} className="card">
      {name ? (
        <h2 className="card-name">{name}</h2>
      ) : (
        <div className="card-image">
          <img src={urlToImage} />
        </div>
      )}

      <div className="card-body">
        <div className="card-date">
          <span>{articleDate}</span>
        </div>

        <div className="card-title">
          <h3>{title}</h3>
        </div>

        <div className="card-excerpt">
          <p>{description}</p>
        </div>
      </div>
    </div>
  );
};
