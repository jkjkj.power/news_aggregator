import { FC } from "react";
import { categoriesSelectOptions, sortSelectOptions } from "../../../constants";
import { Input } from "../Input";
import { Select } from "../Select";

import "./Header.styles.css";

type Props = {
  onSearch: (value: string) => void;
  onSelectSort: (value: string) => void;
  onSelectCategory: (value: string) => void;
  onSelectDate: (date: string) => void;
};

export const Header: FC<Props> = ({
  onSelectSort,
  onSelectCategory,
  onSearch,
  onSelectDate,
}) => {
  return (
    <div className="header">
      <div className="header-tools">
        <h3 className="header-name">News</h3>
        <Input placeholder="Search.." onChange={onSearch} />
        <Input type="date" onChange={onSelectDate} />
        <Select
          defaultOption="Sort by"
          onSelect={onSelectSort}
          options={sortSelectOptions}
        />
        <Select
          defaultOption="Select category"
          onSelect={onSelectCategory}
          options={categoriesSelectOptions}
        />
      </div>
    </div>
  );
};
