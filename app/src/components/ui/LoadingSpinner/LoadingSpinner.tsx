import "./LoadingSpinner.styles.css";

export const LoadingSpinner = () => {
  return <div className="loader"></div>;
};
