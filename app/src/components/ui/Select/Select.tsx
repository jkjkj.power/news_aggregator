import { ChangeEvent, FC } from "react";

import "./Select.styles.css";

type Props = {
  defaultOption?: string;
  options: { id: number; name: string; value: string }[];
  onSelect: (value: string) => void;
};
export const Select: FC<Props> = ({ options, defaultOption, onSelect }) => {
  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    onSelect(event.target.value);
  };

  return (
    <select onChange={handleChange} className="select" name="sort">
      <option value="">{defaultOption ?? "Select something"}</option>
      {options.map(({ name, value, id }) => {
        return (
          <option key={id} value={value}>
            {name}
          </option>
        );
      })}
    </select>
  );
};
