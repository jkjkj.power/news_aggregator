import { Card } from "../ui/Card";
import { Header } from "../ui/Header";
import { useNews } from "../../hooks";
import { LoadingSpinner } from "../ui/LoadingSpinner";

import "./Main.styles.css";

export const Main = () => {
  const {
    news,
    isLoading,
    onSearch,
    onSelectDate,
    onSelectSort,
    onSelectCategory,
  } = useNews();

  return (
    <div>
      <Header
        onSearch={onSearch}
        onSelectDate={onSelectDate}
        onSelectSort={onSelectSort}
        onSelectCategory={onSelectCategory}
      />
      {!news.length && <h2>Type something in search input</h2>}

      {isLoading ? (
        <LoadingSpinner />
      ) : (
        <div className="cards">
          {news.map((article, index) => {
            return <Card key={index} {...article} />;
          })}
        </div>
      )}
    </div>
  );
};
