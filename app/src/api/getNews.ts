import { Endpoints } from "../constants/endpoints";
import fetchRequest from "./fetchRequest";
import { ArticleResponse, TopHeadlinesResponse } from "./types";

export const getNewsByEverypthing = async (searchString: string) => {
  try {
    const res = await fetchRequest<ArticleResponse>(
      searchString,
      Endpoints.EVERYTHING,
    );

    return res.articles.filter(
      (article) =>
        article.title !== "[Removed]" || article.content !== "[Removed]",
    );
  } catch (err) {
    console.error(err);
  }
};

export const getNewsByTopHeadlinesSrc = async (category: string) => {
  try {
    const res = await fetchRequest<TopHeadlinesResponse>(
      category,
      Endpoints.TOP_HEADLINES_SOURCES,
    );

    return res.sources;
  } catch (err) {
    console.error(err);
  }
};
