async function fetchRequest<T = null>(
  params: string,
  endpoint: string,
  init?: RequestInit,
): Promise<T | null> {
  const serverUrl = `https://newsapi.org/v2/${endpoint}?${params}&apiKey=021d863a16be474087884012a675ec0f`;

  const response = await fetch(serverUrl, init);

  if (!response.ok) {
    return Promise.reject(
      new Error(response.status + " (" + response.statusText + ")"),
    );
  }

  const method = init?.method?.toUpperCase();

  if (method === "DELETE" || method === "PATCH") {
    return Promise.resolve(null);
  }

  return (await response.json()) as Promise<T>;
}

export default fetchRequest;
