export type ArticleResponse = {
  status: string;
  totalResults: number;
  articles: Article[];
};

export type TopHeadlinesResponse = {
  status: string;
  sources: TopHeadlines[];
};

export type Article = {
  author: null;
  content: string;
  description: string;
  publishedAt: string;
  source: { id: string | null; name: string };
  title: string;
  url: string;
  urlToImage: string | null;
};

export type TopHeadlines = {
  id: string;
  name: string;
  description: string;
  url: string;
  category: string;
  language: string;
  country: string;
};
