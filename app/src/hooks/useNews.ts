import { useState } from "react";
import { getNewsByEverypthing, getNewsByTopHeadlinesSrc } from "../api";
import { Article, TopHeadlines } from "../api/types";
import { debounce } from "../utils";

const searchParams = new URLSearchParams();

export const useNews = () => {
  const [isLoading, setIsLoading] = useState(false);

  const [news, setNews] = useState<Article[] | TopHeadlines[]>([]);

  const onSearch = debounce((value: string) => {
    if (!value.length) {
      return;
    }
    searchParams.set("q", value);
    getArticles(searchParams.toString());
  }, 500);

  const onSelectDate = (date: string) => {
    searchParams.set("from", date);
    if (!news.length) {
      return;
    }
    getArticles(searchParams.toString());
  };

  const onSelectSort = (value: string) => {
    searchParams.set("sortBy", value);
    if (!news.length) {
      return;
    }
    getArticles(searchParams.toString());
  };

  const onSelectCategory = (value: string) => {
    setIsLoading(true);
    getNewsByTopHeadlinesSrc(value)
      .then(setNews)
      .finally(() => setIsLoading(false));
  };

  const getArticles = (params: string) => {
    setIsLoading(true);

    getNewsByEverypthing(params)
      .then(setNews)
      .finally(() => setIsLoading(false));
  };

  return {
    news,
    isLoading,
    onSearch,
    onSelectDate,
    onSelectSort,
    onSelectCategory,
  };
};
