export enum Endpoints {
  EVERYTHING = "everything",
  TOP_HEADLINES = "top-headlines",
  TOP_HEADLINES_SOURCES = "top-headlines/sources",
}
