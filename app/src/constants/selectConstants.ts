export const sortSelectOptions = [
  { id: 0, name: "Relevancy", value: "relevancy" },
  { id: 1, name: "Popularity", value: "popularity" },
  { id: 2, name: "Published at", value: "publishedAt" },
];

export const categoriesSelectOptions = [
  { id: 0, name: "Business", value: "business" },
  { id: 1, name: "Entertainment", value: "entertainment" },
  { id: 3, name: "General at", value: "general" },
  { id: 4, name: "Health", value: "health" },
  { id: 5, name: "Science", value: "science" },
  { id: 6, name: "Sports", value: "sports" },
  { id: 7, name: "Technology", value: "technology" },
];
